// Generated code from Butter Knife. Do not modify!
package com.ypyglobal.xradio;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class XRadioGrantActivity_ViewBinding implements Unbinder {
  private XRadioGrantActivity target;

  private View view2131231048;

  private View view2131231053;

  private View view2131230776;

  @UiThread
  public XRadioGrantActivity_ViewBinding(XRadioGrantActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public XRadioGrantActivity_ViewBinding(final XRadioGrantActivity target, View source) {
    this.target = target;

    View view;
    target.mTvInfo = Utils.findRequiredViewAsType(source, R.id.tv_info, "field 'mTvInfo'", TextView.class);
    view = Utils.findRequiredView(source, R.id.tv_policy, "method 'onClick'");
    view2131231048 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_tos, "method 'onClick'");
    view2131231053 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
    view = Utils.findRequiredView(source, R.id.btn_allow, "method 'onClick'");
    view2131230776 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onClick(p0);
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    XRadioGrantActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.mTvInfo = null;

    view2131231048.setOnClickListener(null);
    view2131231048 = null;
    view2131231053.setOnClickListener(null);
    view2131231053 = null;
    view2131230776.setOnClickListener(null);
    view2131230776 = null;
  }
}
