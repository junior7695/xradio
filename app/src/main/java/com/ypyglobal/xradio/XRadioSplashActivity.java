/*
 * Copyright (c) 2017. YPY Global - All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *         http://ypyglobal.com/sourcecode/policy
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.ypyglobal.xradio;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.GravityEnum;
import com.afollestad.materialdialogs.MaterialDialog;
import com.wang.avi.AVLoadingIndicatorView;
import com.ypyglobal.xradio.constants.IXRadioConstants;
import com.ypyglobal.xradio.dataMng.TotalDataManager;
import com.ypyglobal.xradio.model.ConfigureModel;
import com.ypyglobal.xradio.model.RadioModel;
import com.ypyglobal.xradio.model.UIConfigModel;
import com.ypyglobal.xradio.setting.XRadioSettingManager;
import com.ypyglobal.xradio.ypylibs.activity.YPYSplashActivity;
import com.ypyglobal.xradio.ypylibs.ads.AdMobAdvertisement;
import com.ypyglobal.xradio.ypylibs.ads.FBAdvertisement;
import com.ypyglobal.xradio.ypylibs.ads.YPYAdvertisement;
import com.ypyglobal.xradio.ypylibs.executor.YPYExecutorSupplier;
import com.ypyglobal.xradio.ypylibs.utils.ApplicationUtils;
import com.ypyglobal.xradio.ypylibs.utils.YPYLog;

import java.io.File;

import butterknife.BindView;



/**
 * @author:YPY Global
 * @Skype: baopfiev_k50
 * @Mobile : +84 983 028 786
 * @Email: bl911vn@gmail.com
 * @Website: www.ypyglobal.com
 * @Date:Oct 20, 2017
 */

public class XRadioSplashActivity extends YPYSplashActivity implements IXRadioConstants{

    public static final String TAG = XRadioSplashActivity.class.getSimpleName();

    @BindView(R.id.progressBar1)
    AVLoadingIndicatorView mProgressBar;

    @BindView(R.id.layout_bg)
    RelativeLayout mLayoutBg;

    private TotalDataManager mTotalMng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        YPYLog.setDebug(DEBUG);
        mTotalMng =TotalDataManager.getInstance();
        setUpBackground(mLayoutBg);
    }

    @Override
    public int getResId() {
        return R.layout.activity_splash;
    }

    @Override
    public void onInitData() {
        mProgressBar.setVisibility(View.VISIBLE);
        mProgressBar.show();
        YPYExecutorSupplier.getInstance().forBackgroundTasks().execute(() -> {
            mTotalMng.readConfigure(this);
            runOnUiThread(() -> {
                setUpBackground(mLayoutBg);
                onStartCreateAds();
            });
            if(isGrantAllPermission(getListPermissionNeedGrant())){
                mTotalMng.readAllCache(this);
            }
            runOnUiThread(() -> showDialogTerm());
        });
    }

    @Override
    public File getDirectoryCached() {
        return mTotalMng.getDirectoryCached();
    }

    @Override
    public String[] getListPermissionNeedGrant() {
        return LIST_PERMISSIONS;
    }


    public void goToMainActivity(boolean isShowAds) {
        boolean isSingleRadio = mTotalMng.isSingleRadio();
        RadioModel mSingleRadio=mTotalMng.getSingRadioModel();
        if(isSingleRadio && mSingleRadio==null){
            boolean isOnline= ApplicationUtils.isOnline(this);
            showToast(isOnline?R.string.info_single_radio_error:R.string.info_connect_to_play);
            return;
        }
        UIConfigModel mUIConfigureModel = mTotalMng.getUiConfigModel();
        boolean isMulti=mUIConfigureModel!=null?mUIConfigureModel.isMultiApp():false;

        boolean b=SHOW_SPLASH_INTERSTITIAL_ADS && SHOW_ADS && isShowAds;
        if(isMulti){
            b=SHOW_SPLASH_INTERSTITIAL_ADS && SHOW_ADS && isShowAds & isGrantAllPermission(getListPermissionNeedGrant());
        }
        showInterstitialAd(b,() -> {
            try{
                if(mProgressBar!=null){
                    mProgressBar.hide();
                    mProgressBar.setVisibility(View.INVISIBLE);
                }
                if(isMulti){
                    if(!isGrantAllPermission(getListPermissionNeedGrant())){
                        Intent mIntent = new Intent(XRadioSplashActivity.this, XRadioGrantActivity.class);
                        startActivity(mIntent);
                        finish();
                    }
                    else{
                        Intent mIntent = new Intent(XRadioSplashActivity.this, XMultiRadioMainActivity.class);
                        startActivity(mIntent);
                        finish();
                    }

                }
                else{
                    Intent mIntent = new Intent(XRadioSplashActivity.this, XSingleRadioMainActivity.class);
                    startActivity(mIntent);
                    finish();
                }
            }
            catch (Exception e){
                e.printStackTrace();
            }
        });

    }


    @Override
    public YPYAdvertisement createAds() {
        ConfigureModel model = mTotalMng.getConfigureModel();
        if(model!=null){
            String bannerId=model.getBannerId();
            String interstitialId=model.getInterstitialId();
            String adType = !TextUtils.isEmpty(model.getAdType()) ?model.getAdType(): AdMobAdvertisement.ADMOB_ADS;
            String appId=model.getAppId();

            if(adType.equalsIgnoreCase(AdMobAdvertisement.ADMOB_ADS)){
                AdMobAdvertisement mAdmob = new AdMobAdvertisement(this,bannerId,interstitialId,ADMOB_TEST_DEVICE );
                mAdmob.setAppId(appId);
                return mAdmob;
            }
            else if(adType.equalsIgnoreCase(FBAdvertisement.FB_ADS)){
                FBAdvertisement mFB = new FBAdvertisement(this, bannerId, interstitialId, FACEBOOK_TEST_DEVICE);
                return mFB;
            }
        }
        return null;

    }

    public void showDialogTerm() {
        if (!XRadioSettingManager.getAgreeTerm(this) && !TextUtils.isEmpty(URL_TERM_OF_USE) && !TextUtils.isEmpty(URL_PRIVACY_POLICY)) {
            try {
                View mView = LayoutInflater.from(this).inflate(R.layout.dialog_term_of_condition, null);
                TextView mTv = mView.findViewById(R.id.tv_term_info);

                String format = getString(R.string.format_term_and_conditional);
                String msg = String.format(format, getString(R.string.app_name), URL_TERM_OF_USE, URL_PRIVACY_POLICY);

                Spanned result;
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                    result = Html.fromHtml(msg, Html.FROM_HTML_MODE_LEGACY);
                }
                else {
                    result = Html.fromHtml(msg);
                }
                mTv.setText(result);
                mTv.setMovementMethod(LinkMovementMethod.getInstance());
                MaterialDialog.Builder mBuilder = createBasicDialogBuilder(R.string.title_term_of_use, R.string.title_agree, R.string.title_no);
                mBuilder.canceledOnTouchOutside(false);
                mBuilder.titleGravity(GravityEnum.CENTER);
                mBuilder.customView(mView, true);
                mBuilder.callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        XRadioSettingManager.setAgreeTerm(XRadioSplashActivity.this, true);
                        goToMainActivity(false);
                    }

                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                        onDestroyData();
                        finish();
                    }
                });
                mBuilder.keyListener((dialogInterface, i, keyEvent) -> {
                    if (i == KeyEvent.KEYCODE_BACK) {
                        return true;
                    }
                    return false;
                });
                mBuilder.show();

            }
            catch (Exception e) {
                e.printStackTrace();
                XRadioSettingManager.setAgreeTerm(XRadioSplashActivity.this, true);
            }
            return;
        }
        goToMainActivity(true);
    }

    private MaterialDialog.Builder createBasicDialogBuilder(int titleId, int resPositive, int resNegative) {
        MaterialDialog.Builder mBuilder = new MaterialDialog.Builder(this);
        mBuilder.backgroundColor(getResources().getColor(R.color.dialog_bg_color));
        mBuilder.title(titleId);
        mBuilder.titleColor(getResources().getColor(R.color.dialog_color_text));
        mBuilder.contentColor(getResources().getColor(R.color.dialog_color_text));
        mBuilder.positiveColor(getResources().getColor(R.color.colorAccent));
        if (resPositive != 0) {
            mBuilder.positiveText(resPositive);
        }
        if (resNegative != 0) {
            mBuilder.negativeText(resNegative);
        }
        mBuilder.negativeColor(getResources().getColor(R.color.dialog_color_secondary_text));
        mBuilder.autoDismiss(true);
        return mBuilder;
    }

}
