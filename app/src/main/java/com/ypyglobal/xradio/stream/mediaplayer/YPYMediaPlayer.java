/*
 * Copyright (c) 2018. YPY Global - All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at.
 *
 *         http://ypyglobal.com/sourcecode/policy
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ypyglobal.xradio.stream.mediaplayer;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.text.TextUtils;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;

import okhttp3.Call;
import okhttp3.OkHttpClient;
import se.greenbird.exoplayershoutcast.ShoutcastDataSourceFactory;


/**
 * @author:dotrungbao
 * @Skype: baopfiev_k50
 * @Mobile : +84 983 028 786
 * @Email: baodt@hanet.com
 * @Website: http://hanet.com/
 * @Project: cyberfm
 * Created by dotrungbao on 5/21/17.
 */

public class YPYMediaPlayer {
    public static final String TAG = YPYMediaPlayer.class.getSimpleName();
    public static final String[] FORMAT_NORMAL_AUDIO = {".mp3",".audio",".wav"};

    private Context mContext;

    private String mUrlStream;
    private String mUserAgent;
    private OnStreamListener onStreamListener;

    private boolean isPrepaired;
    private SimpleExoPlayer mAudioPlayer;
    private final Call.Factory factory = new OkHttpClient.Builder().build();

    @SuppressLint("HandlerLeak")
    public YPYMediaPlayer(Context mContext) {
        this.mContext = mContext;
    }

    public YPYMediaPlayer(Context mContext, String mUserAgent) {
        this(mContext);
        this.mUserAgent = mUserAgent;
    }


    public void release() {
        isPrepaired = false;
        if (mAudioPlayer != null) {
            mAudioPlayer.release();
            mAudioPlayer = null;
        }

    }

    public void setVolume(float volume) {
        try {
            if (mAudioPlayer != null) {
                mAudioPlayer.setVolume(volume);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setOnStreamListener(OnStreamListener onStreamListener) {
        this.onStreamListener = onStreamListener;
    }


    public void setDataSource(String url) {
        if (!TextUtils.isEmpty(url)) {
            this.mUrlStream = url;
            DefaultBandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
            AdaptiveTrackSelection.Factory trackSelectionFactory = new AdaptiveTrackSelection.Factory(bandwidthMeter);
            DefaultTrackSelector trackSelector = new DefaultTrackSelector(trackSelectionFactory);

            mAudioPlayer = ExoPlayerFactory.newSimpleInstance(mContext, trackSelector);
            mAudioPlayer.addListener(new Player.EventListener() {
                @Override
                public void onTimelineChanged(Timeline timeline, Object manifest, int reason) {

                }

                @Override
                public void onTracksChanged(TrackGroupArray trackGroups, TrackSelectionArray trackSelections) {

                }

                @Override
                public void onLoadingChanged(boolean isLoading) {

                }

                @Override
                public void onPlayerStateChanged(boolean playWhenReady, int playbackState) {
                    if (playbackState == Player.STATE_ENDED || playbackState == Player.STATE_IDLE) {
                        if (onStreamListener != null) {
                            onStreamListener.onComplete();
                        }
                    }
                    else if (playbackState == Player.STATE_READY) {
                        if (onStreamListener != null && !isPrepaired) {
                            isPrepaired = true;
                            onStreamListener.onPrepare();
                        }
                    }
                }

                @Override
                public void onRepeatModeChanged(int repeatMode) {

                }

                @Override
                public void onShuffleModeEnabledChanged(boolean shuffleModeEnabled) {

                }

                @Override
                public void onPlayerError(ExoPlaybackException error) {
                    if (onStreamListener != null) {
                        onStreamListener.onError();
                    }
                }

                @Override
                public void onPositionDiscontinuity(int reason) {
                }

                @Override
                public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

                }

                @Override
                public void onSeekProcessed() {

                }
            });
            DataSource.Factory dataSourceFactory;
            MediaSource mediaSource;

            boolean isLive = true;
            for(String mStr:FORMAT_NORMAL_AUDIO){
                if(mUrlStream.toLowerCase().endsWith(mStr)){
                    isLive=false;
                    break;
                }
            }

            if(isLive){
                dataSourceFactory = new ShoutcastDataSourceFactory(factory,
                        getUserAgent(mContext), null, data -> {
                    try{
                        String artist=data.getArtist();
                        String song=data.getSong();
                        String show=data.getShow();
                        StreamInfo mStreamInfo = new StreamInfo();
                        mStreamInfo.title=!TextUtils.isEmpty(song)?song:"";
                        if(TextUtils.isEmpty(mStreamInfo.title)){
                            mStreamInfo.title=!TextUtils.isEmpty(show)?show:"";
                        }
                        mStreamInfo.artist=!TextUtils.isEmpty(artist)?artist:"";
                        if (onStreamListener != null) {
                            onStreamListener.onUpdateMetaData(mStreamInfo);
                        }
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }

                });
            }
            else{
                dataSourceFactory = new DefaultDataSourceFactory(mContext, getUserAgent(mContext), bandwidthMeter);
            }
            if (mUrlStream.endsWith(".m3u8") || mUrlStream.endsWith(".M3U8")) {
                mediaSource = new HlsMediaSource.Factory(dataSourceFactory)
                        .createMediaSource(Uri.parse(mUrlStream));
            }
            else {
                mediaSource = new ExtractorMediaSource.Factory(dataSourceFactory)
                        .setExtractorsFactory(new DefaultExtractorsFactory())
                        .createMediaSource(Uri.parse(mUrlStream));
            }
            mAudioPlayer.prepare(mediaSource);
            start();
            return;

        }
        if (onStreamListener != null) {
            onStreamListener.onError();
        }
    }


    public void start() {
        try {
            if (mAudioPlayer != null) {
                mAudioPlayer.setPlayWhenReady(true);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void pause() {
        try {
            if (mAudioPlayer != null) {
                mAudioPlayer.setPlayWhenReady(false);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

    }

    public boolean isPlaying() {
        try {
            return mAudioPlayer != null && mAudioPlayer.getPlayWhenReady();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public StreamInfo getStreamInfo() {
        try {

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    private String getUserAgent(Context mContext) {
        return Util.getUserAgent(mContext, getClass().getSimpleName());
    }


    public interface OnStreamListener {
        public void onPrepare();
        public void onError();
        public void onComplete();
        public void onBuffering(long percent);
        public void onUpdateMetaData(StreamInfo info);
    }

    public class StreamInfo {
        public String title;
        public String artist;
        public String imgUrl;
    }


}
